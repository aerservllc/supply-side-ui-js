[# SUPPLY SIDE UI -- JAVASCRIPT REPOSITORY

This is the repository we hope to use to house all of our Vue SFCs for the Supply-Side-UI. The goal of this repository is to decouple our Vue SFCs from the main code base to improve readability and functionality.

## SETUP

To start, this repository already has a very small file structure of vue components -- broken down into their various environments (development and production). As I'm still working on the what constitutes the best structure for this repository, it is subject to change. However, this current setup serves as an excellent view into how webpack and the repository work.

The file structure is thusly:

    | Build
      | webpack.config.dev.js //This is the configuration for a development environment
      | webpack.config.prod.js //This is the configuration for a production environment
    | Dist //This is where the configuration files output their builds
    | node_modules //this is not included in the repo and must be loaded locally.
    | src
      | __tests__ //This contains all of the Jest tests (COMING SOON (TM))
      | Components
      [file].js // Various javascript files that use the above components.
    | package-lock.json
    | package.json

Once you have installed node locally on your machine, you will also need to install the following:

    webpack
    webpack-dev-server
    vue-loader
    style-loader
    html-server
    html-webpack-plugin
    babel-core
    jest
    css-loader
    babel-eslint 
    eslint 
    eslint-config-airbnb-base 
    eslint-plugin-import 
    eslint-plugin-vue
    
I believe the quickest way to get these is just to run npm update, which will check the devDependencies section of package.json and install the required files. 

## Configuration

### Entry

When you add a javascript file to this repository and it is not being imported by another resource, the newly-created file must be marked as an entry point in the webpack config relevant to its environment (development, production, or both configs). This is as simple as navigating to the webpack config and adding the file's path to the array marked 'Entry'.

This must be done to ensure Webpack bundles the file into its final result. If the file is being used by another resource (typically through an import statement), then Webpack already knows to include it in the dependency graph, so explicitly marking the path as an entry point is not necessary.

    EXAMPLE
      If I add a new javascript file to src/javascripts called newComponent.js, and it is not imported by any other javascript files declared as entry points in the webpack config, then in webpack.config.dev, within the 'ENTRY' array, I must add the javascript path:
        './src/development/javascripts/newComponent.js'

      HOWEVER

      If I add a new javascript file to src/javascripts and it is imported by another javascript file (say, 'app.js'), then Webpack already knows to include it in its dependency graph, so you do not need to explicitly specify it as an entry point.

### Rules

    Rules specify what loaders to use for filetypes. If you need a file to use a specific type of loader, just set a rule for it (see current rules in the webpack.config for examples).

### Externals

Want to use files by loading in a CDN? Hate having to load ALL of the source code? Me too! Luckily, webpack allows you to declare externals. This tells Webpack to avoid building their data, because it assumes you are going to load it in from a CDN.

      EXAMPLE:
        In webpack.config.prod.js:
          externals: {
            vue: "Vue" // This tells Webpack 'Don't load in the vue source, I'm going to do that in my code (typically through a CDN).
          },

You can use externals with any library, including jQuery, Axios, etc...

## Running The Build

In the package.json file, I have provided two scripts to run webpack, based on development environment

    scripts: {
        "build:dev": ...
        "build:prod": ...
    }

You can call these scripts with the 'npm run' command

      'npm run build:dev' or 'npm run build:prod'

This will call the development version of webpack config (and build:prod calls the production version).

After calling this command, webpack will bundle your code into a single javascript file called 'main-dev.js' for the development environment, and 'main-prod.js' for the production environment.

### With Hot-Reloading

Currently, the 'watch' property of webpack is turned on for the development environment. This means webpack will re-build the code after it detects changes. This freezes the current terminal window, so you will have to create a second window if you want to enable hot reloading and the server.

## Hosting the Build

To host the build on your localhost:8000, simply call "npm start". This will start a server on your localhost:8000, which will hold all of your built code to be used by the SSUI. To use this code in the SSUI environment, simply use the following script tag:

      <script src = "http://localhost:8000/dist/main-dev.js" type = "text/javascript"></script>
](# SUPPLY SIDE UI -- JAVASCRIPT REPOSITORY

This is the repository we hope to use to house all of our Vue SFCs for the Supply-Side-UI. The goal of this repository is to decouple our Vue SFCs from the main code base to improve readability and functionality.

## SETUP

To start, this repository already has a very small file structure of vue components -- broken down into their various environments (development and production). As I'm still working on the what constitutes the best structure for this repository, it is subject to change. However, this current setup serves as an excellent view into how webpack and the repository work.

The file structure is thusly:

    | Build
      | webpack.config.dev.js OR //This is the configuration for a development environment
      | webpack.config.prod.js //This is the configuration for a production environment
    | Dist //This is where the configuration files output their builds
    | node_modules //this is not included in the repo and must be loaded locally.
    | src
      | components // This contains all of the vue components written
      -[file].js //the js files that use the vue components
    
    

Once you have installed node locally on your machine, you will also need to install the following:

    webpack
    vue-loader
    style-loader
    html-server
    html-webpack-plugin
    @babel/core
    jest
    css-loader

## Configuration

### Entry

When you add a javascript file to this repository and it is not being imported by another resource, the newly-created file must be marked as an entry point in the webpack config relevant to its environment (development, production, or both configs). This is as simple as navigating to the webpack config and adding the file's path to the array marked 'Entry'.

This must be done to ensure Webpack bundles the file into its final result. If the file is being used by another resource (typically through an import statement), then Webpack already knows to include it in the dependency graph, so explicitly marking the path as an entry point is not necessary.

    EXAMPLE
      If I add a new javascript file to development/javascripts called newComponent.js, and it is not imported by any other javascript files declared as entry points in the webpack config, then in webpack.config.dev, within the 'ENTRY' array, I must add the javascript path:
       './src/development/javascripts/newComponent.js'

      HOWEVER

      If I add a new javascript file to development/javascripts and it is imported by another javascript file (say, 'app.js'), then Webpack already knows to include it in its dependency graph, so you do not need to explicitly specify it as an entry point.


### Rules

    Rules specify what loaders to use for filetypes. If you need a file to use a specific type of loader, just set a rule for it (see current rules in the webpack.config for examples).

### Externals

Want to use files by loading in a CDN? Hate having to load ALL of the source code? Me too! Luckily, webpack allows you to declare externals. This tells Webpack to avoid building their data, because it assumes you are going to load it in from a CDN.

      EXAMPLE:
        In webpack.config.prod.js:
          externals: {
            vue: "Vue" // This tells Webpack 'Don't load in the vue source, I'm going to do that in my code (typically through a CDN)'.
          },

You can use externals with any library, including jQuery, Axios, etc...

## Running The Build

To build the file, simply run the following command:
    
      npm run build:dev

      or

      npm run build:prod
      
This will call the appropriate webpack config file.

After calling this command, webpack will bundle your code into a single javascript file called 'main-dev.js' for the development environment, and 'main-prod.js' for the production environment.

### With Watching

Currently, the 'watch' property of webpack is turned on for the development environment. This means webpack will re-build the code after it detects changes. This freezes the current terminal window, so you will have to create a second window if you want to simultaneously enable automatic re-building and the npm server. 

## Hosting the Build

To host the build on your localhost:8000, simply call "npm start". This will start a server on your localhost:8000, which will hold all of your built code to be used by the SSUI. To use this code in the SSUI environment, simply use the following script tag:

      <script src = "http://localhost:8000/dist/main-[dev/prod].js" type = "text/javascript"></script>

## Misc:

While testing your code, it helps to ensure the developer tools window is open and 'Disable Cache' is selected. This way, when you change your code and refresh the page, it's accessing the latest data. It is the best way to ensure you are seeing the newest changes made to the components. 