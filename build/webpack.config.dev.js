const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  mode: 'development',
  entry: [
    './src/app.js',
    './src/direct-campaign.js',
   './src/alert.js',
   './src/sitereportform.js',
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  watch: true,
  devtool: 'source-map',
  output: {
    filename: 'main-dev.js',
  },
  plugins: [
    new VueLoaderPlugin(),
  ],
};
