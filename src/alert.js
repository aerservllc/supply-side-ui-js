import Vue from "vue";
import alert from "./components/Alert.vue";

if (document.getElementById("alert")) {
  document.addEventListener("DOMContentLoaded", () => {
    var vm1 = new Vue({
      el: "#alert",
      render: h => h(alert)
    });
  });
}
