import Vue from "vue";
import app from "./components/app.vue";

if (document.getElementById("app")) {
  document.addEventListener("DOMContentLoaded", () => {
    var vm1 = new Vue({
      el: "#app",
      render: h => h(app)
    });
  });
}
