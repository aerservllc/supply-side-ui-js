import Vue from "vue";
import DC from "./components/directcampaign.vue";

if (document.getElementById("direct-campaign")) {
  document.addEventListener("DOMContentLoaded", () => {
    var vm1 = new Vue({
      el: "#direct-campaign",
      render: h => h(DC)
    });
  });
}
