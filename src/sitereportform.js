import Vue from "vue";
import SiteReportForm from "./components/SiteReportForm.vue";

if (document.getElementById("site-report-form")) {
  document.addEventListener("DOMContentLoaded", () => {
    var vm1 = new Vue({
      el: "#site-report-form",
      render: h => h(SiteReportForm)
    });
  });
}
